import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

/**
 * Alcancia
 */
public class Alcancia {

  private int amount = 0;
  private Map<Integer, Integer> coins;
  private int badCoins;

  public Alcancia() {
    coins = new HashMap<>();
    this.badCoins = 0;
  }

  public static void main(String[] args) {
    Alcancia alcancia = new Alcancia();
    alcancia.process();

    alcancia.printCoins();
  }


  private void printCoins() {
    System.out.println("Monedas de 100: " + this.coins.get(100));
    System.out.println("Monedas de 200: " + this.coins.get(200));
    System.out.println("Monedas de 500: " + this.coins.get(500));
    System.out.println("Monedas de 1000: " + this.coins.get(1000));
        
    System.out.println("Número de monedas malas: " + this.badCoins);
  }

  private void process() {
    //verificar estado lleno
    while (!isFull()) {
      //Solicitar inserción de moneda  
      String input = JOptionPane.showInputDialog(null, "Introducir moneda [100,200,500,1000]");
      if ("100".equals(input) || "200".equals(input)||"500".equals(input)||"1000".equals(input)) {
        int coin = Integer.valueOf(input);

        //Actualizar/introducir en la alcancia
        push(coin);
      } else if("X".equals(input)) {
        break;
      } else {
        this.badCoins++;
      }
    }
  }

  private boolean isFull() {
    return amount >= 5000;
  }

  private void push(int coin) {
    if (amount + coin <= 5000) {
      this.amount += coin;
      if (this.coins.containsKey(coin)) {
        this.coins.put(coin, this.coins.get(coin) + 1);
      } else {
        this.coins.put(coin, 1);
      }
    } else {
      System.out.println("No es posible introducir la moneda");
    }
  }
}