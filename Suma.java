import javax.swing.JOptionPane;

/**
 * Suma
 */
public class Suma {

    public static int sum(int num1, int num2) {
        return num1 + num2;
    }

    public static void main(String[] args) {
        //1. Pedir dos nùmeros
        String s1 = JOptionPane.showInputDialog(null, "Por favor, introduce número 1: ");
        int num1 = Integer.valueOf(s1);

        String s2 = JOptionPane.showInputDialog(null, "Por favor, introduce número 2: ");
        int num2 = Integer.valueOf(s2);

        //2. Suma
        int ans = sum(num1, num2);

        //3. Mostrar resultado
        System.out.println("Resultado: " + ans);
        JOptionPane.showMessageDialog(null, "El resultado es: " + ans);
    }
}