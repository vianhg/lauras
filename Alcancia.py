
class Alcancia:
  amount = 0
  coins = { 100:0, 200:0, 500:0, 1000:0}
  badCoins = 0

  def isFull(self):
    return self.amount >= 5000

  def push(self, coin):
    if (self.amount + coin <= 5000):
      self.amount += coin
      self.coins[coin] += 1
      print(self.amount)
    else:
      print("No es posible introducir la moneda")

  def process(self):
    while(not self.isFull()):
      inp = input("Introducir moneda [100,200,500,1000]: ")
      if (inp == '100' or inp == '200' or inp == '500' or inp == '1000'):
        self.push(int(inp))
      elif (inp == 'X'):
          break
      else:
          self.badCoins+=1
      

  def printCoins(self):
    print("Monedas: ", self.coins)
    print("Moneas malas: ", self.badCoins)

alcancia = Alcancia()
alcancia.process()
alcancia.printCoins()