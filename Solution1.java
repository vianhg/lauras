import javax.swing.JOptionPane;

//POO Prog Orientada a Objetos
/**
 * Paso 1: crear la clase: Solution1
 */
public class Solution1 {
    // Variables
    private String nombre;
    private int tamano;
    private String id;
    private String owner; 

    // Funciones
    public String barf() {
        return "Uauf";
    }

    public void bite() {
        System.out.println("mordido");
    }

    public static void main(String[] args) {
        // Condicionales
        int num1 = 10;
        int num2 = 0;
        if (num1 > num2) {
            System.out.println("El mayor es " + num1);
        } else {
            System.out.println("El mayor es " + num2);
        }

        // Ciclos
        int i = 1;
        while (i <= 10) {
            System.out.println("Paso " + i);
            i = i + 1;
        }

        for (int j = 0; j < 10; j++) {
            System.out.println(j);
        }

        //Solicitar datos
        String input = JOptionPane.showInputDialog(null, "Por favor ingresa tu sexo: ");
        System.out.println("Hello " + input);
        JOptionPane.showMessageDialog(null, "Mostrar un ventana con info.");
    }
}
