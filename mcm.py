#mcm 10 y 20
# 10, *20, 30, 40..
# *20, 40, 60, 80 ...

def mcm(a, b):
  for i in range(1, b+1):
    ans = a * i
    if (ans % b == 0):
      return ans
  return -1

print(mcm(72, 50))